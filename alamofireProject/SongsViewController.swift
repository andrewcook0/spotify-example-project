//
//  PostsViewController.swift
//  alamofireProject
//
//  Created by andrew cook on 9/22/16.
//  Copyright © 2016 andrew cook. All rights reserved.
//

import UIKit
import AVFoundation

class SongsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView : UITableView!

    var player = AVAudioPlayer()
    var postsArray : NSArray! = NSArray()
    var postsCell : SongsCell! = SongsCell()
    var album : NSArray!
    var playlist : NSArray!
    var whichType : ViewController.TypeOfSearch!
    
    //MARK: ViewDidLoad
    override func viewDidLoad()
    {
        if whichType == ViewController.TypeOfSearch.album
        {
            APICommunicator.sharedCommunicator.getSongsForAlbum(album[2] as! String) { (songs) in
                self.album = songs
                self.tableView.reloadData()
            }
        }
        else if whichType == ViewController.TypeOfSearch.playlist
        {
            APICommunicator.sharedCommunicator.getSongsForPlaylist(playlist[2] as! String,userID: playlist[3] as! String, completion: { (songs) in
                self.playlist = songs
                self.tableView.reloadData()
            })
        }
        let seconds = 1.0
        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
        let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            
            self.tableView.reloadData()
            
        })
    }
    
    //MARK: TableView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if whichType == ViewController.TypeOfSearch.album
        {
            return self.album.count
        }
        else if whichType == ViewController.TypeOfSearch.playlist
        {
            return self.playlist.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var externalURL = String()
        var url = String()
        
        if whichType == ViewController.TypeOfSearch.album
        {
        let song = album.object(at: (indexPath as NSIndexPath).row) as! NSArray
        url = song[1] as! String
        externalURL = song[2] as! String
        }
        else if whichType == ViewController.TypeOfSearch.playlist
        {
            let song = playlist.object(at: (indexPath as NSIndexPath).row) as! NSArray
            url = song[1] as! String
            externalURL = song[2] as! String
        }
        
        
        let alert = UIAlertController(title: "Choose an option.", message: "Would you like to play the full version or listen to a sample?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Full Version", style: UIAlertActionStyle.default, handler: { action in
            switch action.style{
            case .default:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "webView") as! WebView
                vc.externalURL = externalURL
                self.navigationController?.pushViewController(vc, animated: true)
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Sample Version", style: UIAlertActionStyle.default, handler: { action in
            switch action.style{
            case .default:
                print(externalURL)
                if url != ""
                {
                self.downloadFileFromURL(URL(string: url)!)
                }
                else
                {
                    print("has no sample url")
                }
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        tableView.register(UINib(nibName: "SongsCell", bundle: nil), forCellReuseIdentifier: "SongsCell")
        postsCell = tableView.dequeueReusableCell(withIdentifier: "SongsCell") as! SongsCell
      
        if whichType == ViewController.TypeOfSearch.album
        {
            let song = album.object(at: (indexPath as NSIndexPath).row) as! NSArray
            let songName = song[0] as? String
            postsCell.titleLabel.text = songName
        }
        else if whichType == ViewController.TypeOfSearch.playlist
        {
            let song = playlist.object(at: (indexPath as NSIndexPath).row) as! NSArray
            print(song)
            let songName = song[0] as? String
            postsCell.titleLabel.text = songName

        }
        
        return postsCell
    }
    
    //MARK: Actions
    
    @IBAction func backButtonClicked()
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK: Play Songs
    
    func play(_ url:URL) {
        print("playing \(url)")
        
        do {
            self.player = try AVAudioPlayer(contentsOf: url)
            player.prepareToPlay()
            player.volume = 1.0
            player.play()
        } catch let error as NSError {
            //self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
    }
    
    func downloadFileFromURL(_ url:URL){
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: { (URL, response, error) -> Void in
            
            self.play(URL!)
            
        })
        
        downloadTask.resume()
        
    }
    
    
    
    
}
