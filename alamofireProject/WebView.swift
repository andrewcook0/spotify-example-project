//
//  WebView.swift
//  alamofireProject
//
//  Created by andrew cook on 9/28/16.
//  Copyright © 2016 andrew cook. All rights reserved.
//

import UIKit

class WebView: UIViewController, UIWebViewDelegate
{
    var externalURL = String()
    
    override func viewDidLoad()
    {
        if externalURL == ""
        {
            externalURL = "https://accounts.spotify.com/authorize?client_id=255dab98267c44e5a6f6c22921c4f824&redirect_uri=http%3A//mysite.com/callback/&response_type=token"
        }
        
        let webView = UIWebView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        webView.loadRequest(URLRequest(url: URL(string: "\(externalURL)")!))
        self.view.addSubview(webView)
        webView.delegate = self
        self.view.sendSubview(toBack: webView)
    }
    
    @IBAction func backButtonClicked()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if (webView.request?.url?.absoluteString.contains("#access_token") == true)
        {
            let needsStrippingString = webView.request?.url?.absoluteString
            let newStrippedString = needsStrippingString?.replace("http://mysite.com/callback/#access_token=", withString: "")
            let accessToken = newStrippedString?.replace("&token_type=Bearer&expires_in=3600", withString: "")
            APICommunicator.sharedCommunicator.accessToken = accessToken!
            
            UserDefaults.standard.set(accessToken, forKey: "authToken")
            UserDefaults.standard.synchronize()
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhotoViewController")
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
    }
}
