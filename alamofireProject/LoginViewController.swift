//
//  LoginViewController.swift
//  alamofireProject
//
//  Created by andrew cook on 9/21/16.
//  Copyright © 2016 andrew cook. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate, UIWebViewDelegate
{
    var mainView : UIView!
    var secondaryView : UIView!
    var goToLoginViewButton : UIButton!
    var goToCreateAccountButton : UIButton!
    var cancelButton : UIButton!
    var loginButton : UIButton!
    var createAccountButton : UIButton!
    var emailTextField : UITextField!
    var usernameTextField : UITextField!
    var passwordTextField : UITextField!
    var previousPosition : CGRect!
    var keyboardHeight = CGFloat()
    var firstTime : Bool = true
    
    //MARK: ViewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        self.mainView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        self.secondaryView = UIView.init(frame:CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height))
        self.goToLoginViewButton = UIButton.init(frame: CGRect(x: -2, y: self.view.frame.height - 60, width: self.view.frame.width + 4, height: 60))
        self.goToCreateAccountButton = UIButton.init(frame: CGRect(x: -2, y: self.view.frame.height - 122, width: self.view.frame.width + 4, height: 60))
        self.loginButton = UIButton.init(frame: CGRect(x: -2, y: self.view.frame.height - 122, width: self.view.frame.width + 4, height: 60))
        self.createAccountButton = UIButton.init(frame: CGRect(x: -2, y: self.view.frame.height - 122, width: self.view.frame.width + 4, height: 60))
        self.cancelButton = UIButton.init(frame: CGRect(x: -2, y: self.view.frame.height - 60, width: self.view.frame.width + 4, height: 60))
        self.passwordTextField = UITextField.init(frame: CGRect(x: -2, y: loginButton.frame.origin.y - 80, width: self.view.frame.width + 4, height: 60))
        self.usernameTextField = UITextField.init(frame: CGRect(x: -2, y: passwordTextField.frame.origin.y - 62, width: self.view.frame.width + 4, height: 60))
        self.emailTextField = UITextField.init(frame: CGRect(x: -2, y: usernameTextField.frame.origin.y - 62, width: self.view.frame.width + 4, height: 60))
        
        goToLoginViewButton.layer.borderColor = UIColor.black.cgColor
        goToLoginViewButton.backgroundColor = UIColor(red: 40/255, green: 56/255, blue: 0/255, alpha: 1.0)
        goToLoginViewButton.titleLabel!.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        goToLoginViewButton.setTitle("Login", for: UIControlState())
        goToLoginViewButton.layer.borderWidth = 2
        goToLoginViewButton.addTarget(self, action: #selector(goToLoginView), for: .touchUpInside)
        
        goToCreateAccountButton.layer.borderColor = UIColor.black.cgColor
        goToCreateAccountButton.backgroundColor = UIColor(red: 40/255, green: 56/255, blue: 0/255, alpha: 1.0)
        goToCreateAccountButton.titleLabel!.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        goToCreateAccountButton.setTitle("Create Account", for: UIControlState())
        goToCreateAccountButton.layer.borderWidth = 2
        goToCreateAccountButton.addTarget(self, action: #selector(goToCreateAccountView), for: .touchUpInside)
        
        cancelButton.layer.borderColor = UIColor.black.cgColor
        cancelButton.backgroundColor = UIColor(red: 40/255, green: 56/255, blue: 0/255, alpha: 1.0)
        cancelButton.titleLabel!.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        cancelButton.setTitle("Cancel", for: UIControlState())
        cancelButton.layer.borderWidth = 2
        cancelButton.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
        
        createAccountButton.layer.borderColor = UIColor.black.cgColor
        createAccountButton.backgroundColor = UIColor(red: 40/255, green: 56/255, blue: 0/255, alpha: 1.0)
        createAccountButton.titleLabel!.font =  UIFont(name: "HelveticaNeue-Bold", size: 20)
        createAccountButton.setTitle("Create Account", for: UIControlState())
        createAccountButton.layer.borderWidth = 2
        createAccountButton.addTarget(self, action: #selector(createAccount), for: .touchUpInside)
        
        loginButton.layer.borderColor = UIColor.black.cgColor
        loginButton.backgroundColor = UIColor(red: 40/255, green: 56/255, blue: 0/255, alpha: 1.0)
        loginButton.titleLabel!.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        loginButton.setTitle("Login", for: UIControlState())
        loginButton.layer.borderWidth = 2
        loginButton.addTarget(self, action: #selector(login), for: .touchUpInside)
        
        emailTextField.layer.borderColor = UIColor.black.cgColor
        emailTextField.backgroundColor = UIColor.white
        emailTextField.font = UIFont(name: "HelveticaNeue-Light", size: 15)
        emailTextField.placeholder = "Email"
        emailTextField.layer.borderWidth = 2
        emailTextField.textAlignment = .center
        emailTextField.delegate = self
        
        usernameTextField.layer.borderColor = UIColor.black.cgColor
        usernameTextField.backgroundColor = UIColor.white
        usernameTextField.font = UIFont(name: "HelveticaNeue-Light", size: 15)
        usernameTextField.placeholder = "Username"
        usernameTextField.layer.borderWidth = 2
        usernameTextField.textAlignment = .center
        usernameTextField.delegate = self
        
        passwordTextField.layer.borderColor = UIColor.black.cgColor
        passwordTextField.backgroundColor = UIColor.white
        passwordTextField.font = UIFont(name: "HelveticaNeue-Light", size: 15)
        passwordTextField.placeholder = "Password"
        passwordTextField.layer.borderWidth = 2
        passwordTextField.textAlignment = .center
        passwordTextField.delegate = self
        
        self.view.addSubview(mainView)
        self.view.addSubview(secondaryView)
        self.mainView.addSubview(goToLoginViewButton)
        self.mainView.addSubview(goToCreateAccountButton)
        self.secondaryView.addSubview(self.emailTextField)
        self.secondaryView.addSubview(self.usernameTextField)
        self.secondaryView.addSubview(self.passwordTextField)
        self.secondaryView.addSubview(self.loginButton)
        self.secondaryView.addSubview(self.createAccountButton)
        self.secondaryView.addSubview(self.cancelButton)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Actions
    
    func goToLoginView()
    {
        UIView.animate(withDuration: 0.5, animations:
            {
                self.mainView.frame.origin.y = 0 - self.view.frame.height
                self.secondaryView.frame.origin.y = 0
                self.loginButton.isHidden = false
                self.createAccountButton.isHidden = true
                self.emailTextField.isHidden = true
                
            }, completion:
            {
                (value: Bool) in
                
        })
    }
    
    func goToCreateAccountView()
    {
        UIView.animate(withDuration: 0.5, animations:
            {
                self.mainView.frame.origin.y = 0 - self.view.frame.height
                self.secondaryView.frame.origin.y = 0
                self.loginButton.isHidden = true
                self.createAccountButton.isHidden = false
                self.emailTextField.isHidden = false
                
            }, completion:
            {
                (value: Bool) in
                
        })
    }
    
    func backButtonClicked()
    {
        UIView.animate(withDuration: 0.5, animations:
            {
                self.mainView.frame.origin.y = 0
                self.secondaryView.frame.origin.y = self.view.frame.height
                
                self.view.bringSubview(toFront: self.mainView)
                
            }, completion:
            {
                (value: Bool) in
                
        })
        
    }
    
    func createAccount()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController")
        self.navigationController!.pushViewController(vc!, animated: true)
    }
    
    func login()
    {
//        APICommunicator.sharedCommunicator.loginUser(usernameTextField.text!) { (success) -> Void in
//            if success == true
//            {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhotoViewController")
//                self.navigationController?.pushViewController(vc!, animated: true)
//                
//                UserDefaults.standard.set(self.usernameTextField.text, forKey: "username")
//                UserDefaults.standard.synchronize()
//            }
//            else
//            {
//                print(success)
//            }
//        }
        
    }
    
    //MARK: Text Field
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if firstTime == true
        {
            //needs delay first time to set keyboard height
            UIView.animate(withDuration: 0.1, animations:
                {
                    
                }, completion:
                {
                    (value: Bool) in
                    
                    if self.emailTextField.isFirstResponder
                    {
                        self.previousPosition = self.emailTextField.frame
                        UIView.animate(withDuration: 0.3, animations:
                            {
                                self.emailTextField.frame.origin.y = self.view.frame.height - self.keyboardHeight - 60
                            }, completion:
                            {
                                (value: Bool) in
                                
                        })
                        
                    }
                    else if self.usernameTextField.isFirstResponder
                    {
                        self.previousPosition = self.usernameTextField.frame
                        UIView.animate(withDuration: 0.3, animations:
                            {
                                self.usernameTextField.frame.origin.y = self.view.frame.height - self.keyboardHeight - 60
                            }, completion:
                            {
                                (value: Bool) in
                                
                        })
                        
                    }
                    else if self.passwordTextField.isFirstResponder
                    {
                        self.previousPosition = self.passwordTextField.frame
                        UIView.animate(withDuration: 0.3, animations:
                            {
                                self.passwordTextField.frame.origin.y = self.view.frame.height - self.keyboardHeight - 60
                            }, completion:
                            {
                                (value: Bool) in
                                
                        })
                        
                    }
            })
        }
        else
        {
            if emailTextField.isFirstResponder
            {
                previousPosition = emailTextField.frame
                UIView.animate(withDuration: 0.3, animations:
                    {
                        self.emailTextField.frame.origin.y = self.view.frame.height - self.keyboardHeight - 60
                    }, completion:
                    {
                        (value: Bool) in
                        
                })
                
            }
            else if usernameTextField.isFirstResponder
            {
                previousPosition = usernameTextField.frame
                UIView.animate(withDuration: 0.3, animations:
                    {
                        self.usernameTextField.frame.origin.y = self.view.frame.height - self.keyboardHeight - 60
                    }, completion:
                    {
                        (value: Bool) in
                        
                })
                
            }
            else if passwordTextField.isFirstResponder
            {
                previousPosition = passwordTextField.frame
                UIView.animate(withDuration: 0.3, animations:
                    {
                        self.passwordTextField.frame.origin.y = self.view.frame.height - self.keyboardHeight - 60
                    }, completion:
                    {
                        (value: Bool) in
                        
                })
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == emailTextField
        {
            UIView.animate(withDuration: 0.3, animations:
                {
                    self.emailTextField.frame = self.previousPosition
                }, completion:
                {
                    (value: Bool) in
            })
        }
        else if textField == usernameTextField
        {
            UIView.animate(withDuration: 0.3, animations:
                {
                    self.usernameTextField.frame = self.previousPosition
                }, completion:
                {
                    (value: Bool) in
            })
        }
        else if textField == passwordTextField
        {
            UIView.animate(withDuration: 0.3, animations:
                {
                    self.passwordTextField.frame = self.previousPosition
                }, completion:
                {
                    (value: Bool) in
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.endEditing(true)
        return true
    }
    
    func keyboardWillShow(_ notification:Notification)
    {
        let userInfo:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        keyboardHeight = keyboardRectangle.height
    }
    
    //MARK: WebView Delegate
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        
    }
    
}
