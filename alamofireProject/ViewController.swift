//
//  ViewController.swift
//  alamofireProject
//
//  Created by andrew cook on 8/29/16.
//  Copyright © 2016 andrew cook. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate
{
    @IBOutlet weak var collectionView : UICollectionView!
    
    var searchField = UITextField()
    var playlistArray = NSArray()
    var cell = ArtworkCell()
    var searchString : String!
    var keyboardHeight = CGFloat()
    var whichType : TypeOfSearch!
    var keyboardFirstTime = true
    
    var firstString : String!
    var modifiedString : String!
    
    enum TypeOfSearch {
        case album, playlist
    }
    
    //MARK: ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let searchRect = CGRect(x: 0, y: self.view.frame.height - 51, width: self.view.frame.width, height: 51)
        searchField = UITextField.init(frame: searchRect)
        searchField.layer.borderColor = UIColor.black.cgColor
        searchField.backgroundColor = UIColor.white
        searchField.layer.borderWidth = 1
        searchField.textAlignment = .center
        searchField.placeholder = "Enter search term"
        searchField.delegate = self
        self.view.addSubview(searchField)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: CollectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.playlistArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        collectionView.register(UINib(nibName: "ArtworkCell", bundle: nil), forCellWithReuseIdentifier: "ArtworkCell")
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtworkCell", for: indexPath) as! ArtworkCell
        
        let tempName = self.playlistArray.object(at: indexPath.row)
        let name = (tempName as AnyObject).object(at: 0)
        
        self.cell.playlistName.text = name as? String
        
        let tempURL = self.playlistArray.object(at: indexPath.row)
        let url = (tempURL as AnyObject).object(at: 1)
        if let url = URL(string: url as! String)
        {
            self.cell.imageView.downloadedFrom(url: url)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    {
            return CGSize(width: self.view.frame.size.width / 3 - 7, height: self.view.frame.size.width / 3 - 7)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostsViewController") as! SongsViewController
        if whichType == .album
        {
            vc.album = playlistArray.object(at: (indexPath as NSIndexPath).row) as! NSArray
            vc.whichType = .album
        }
        else if whichType == .playlist
        {
            vc.playlist = playlistArray.object(at: (indexPath as NSIndexPath).row) as! NSArray
            vc.whichType = .playlist
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: TextField Delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        searchField.frame.origin.y = self.view.frame.height - searchField.frame.size.height
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        //adds .01 delay to wait and assign keyboard height for the first time
        if keyboardFirstTime == true
        {
            let when = DispatchTime.now() + 0.01
            DispatchQueue.main.asyncAfter(deadline: when)
            {
                UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseOut], animations:
                    {
                    self.searchField.frame.origin.y = self.view.frame.height - self.keyboardHeight - self.searchField.frame.size.height
                    }, completion: nil)
            }
            keyboardFirstTime = false
        }
        else
        {
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations:
                {
                    self.searchField.frame.origin.y = self.view.frame.height - self.keyboardHeight - self.searchField.frame.size.height
                }, completion: nil)
        }
        
        textField.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.endEditing(true)
        
        if textField.text != ""
        {
            firstString = textField.text
            modifiedString = firstString!.replace(" ", withString:"%20")
            
            let alert = UIAlertController(title: "Choose a search type.", message: "Would you like to search albums or playlists?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Album", style: UIAlertActionStyle.default, handler: { action in
                switch action.style{
                case .default:
                    self.whichType = .album
                    self.performSearch()
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Playlist", style: UIAlertActionStyle.default, handler: { action in
                switch action.style{
                case .default:
                    self.whichType = .playlist
                    self.performSearch()
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
        return true
    }
    
    //MARK: Actions
    
    func performSearch()
    {
        if whichType == .album
        {
            APICommunicator.sharedCommunicator.getAlbums(modifiedString, completion: { (albums) in
                self.playlistArray = albums
                self.collectionView.reloadData()
            })
        }
        else if whichType == .playlist
        {
            APICommunicator.sharedCommunicator.getPlaylists(modifiedString) { (playlists) in
                self.playlistArray = playlists
                self.collectionView.reloadData()
            }
        }
    }
    
    func keyboardWillShow(_ notification:Notification)
    {
        let userInfo:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        keyboardHeight = keyboardRectangle.height
    }
}








