//
//  Cell.swift
//  
//
//  Created by andrew cook on 8/30/16.
//
//

import UIKit

class ArtworkCell: UICollectionViewCell
{
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var playlistName : UILabel!
}
