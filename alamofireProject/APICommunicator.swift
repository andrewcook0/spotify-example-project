//
//  APICommunicator.swift
//  
//
//  Created by andrew cook on 8/30/16.
//
//

import UIKit
import Alamofire

class APICommunicator
{
    static let sharedCommunicator = APICommunicator()
    
    var clientID = "255dab98267c44e5a6f6c22921c4f824"
    var clientSecret = "e355f4738a324ef690bf5caf15ee4dfd"
    var accessToken = String()
    
    let root = "https://api.spotify.com"
    let otherRoot = "http://jsonplaceholder.typicode.com"
    let apiKey = "LSHpd3i3SQ2k7xuCeKcWS88dOGvn5dNn4Nm64yaZ"
    let search = "/v1/search?q="
    
    //MARK: Authorize User
    
    //gets user info, if failure grabs new access token
    func getUser(_ previousToken:String,completion: (_ token:String) -> Void)
    {
        Alamofire.request(APICommunicator.sharedCommunicator.root + "/v1/me", encoding: URLEncoding.default, headers: ["Authorization" : "Bearer " + accessToken]) .responseJSON { response in
            
            let object = response.result.value
            let tempError = (object as! NSDictionary).object(forKey: "error")
            
            if tempError != nil
            {
                Alamofire.request("https://accounts.spotify.com/api/token",
                                  method: .post,
                                  parameters: ["grant_type":"client_credentials", "code":self.accessToken,"redirect_uri":"http://mysite.com/callback/"],
                                  encoding: URLEncoding.default,
                                  headers: ["Authorization":"Basic " + ((self.clientID + ":" + self.clientSecret).toBase64())]).responseJSON { response in
                        
                        if response.result.isFailure
                        {
                            print(response.result)
                            print(response.request)
                            print(response.description)
                        }
                        else
                        {
                            print(response)
                            let object = response.result.value
                            let tempAccessToken = (object as! NSDictionary).object(forKey: "access_token")
                            
                            self.accessToken = tempAccessToken as! String
                            
                            UserDefaults.standard.set(tempAccessToken!, forKey: "authToken")
                            UserDefaults.standard.synchronize()
                        }
                }
            }
            else
            {
                print(response)
            }
        }
    }
    
    func authUser(_ completion:(_ token:String) -> Void)
    {
        Alamofire.request("https://accounts.spotify.com/authorize", parameters: ["client_id" : clientID, "redirect_uri" : "http://mysite.com/callback/", "response_type" : "token",], encoding: URLEncoding.default) .responseJSON { response in
            
            if response.result.isFailure
            {
                print(response.result)
                print(response.request)
                print(response.description)
            }
            else
            {                
                print(response)
            }
        }
    }
    
    //MARK: Get Objects
    
    func getSongsForPlaylist(_ id:String,userID:String, completion:@escaping (_ songs:NSArray) -> Void)
    {
        let fullArray = NSMutableArray()
        let headers = ["content-type": "application/json","cache-control": "no-cache","Authorization": "Bearer " + accessToken]
        var name = String()
        var url = String()
        var externalURL = String()
        
        let strippedUserID = userID.replace("spotify:user:", withString: "")
        let finalStrippedID = strippedUserID.replace(":playlist:" + id, withString: "")
        
        Alamofire.request(root + "/v1/users/" + finalStrippedID + "/playlists/" + id + "/tracks",
                          parameters: ["":""],
                          encoding: URLEncoding.default,
                           headers: headers) .responseJSON { response in
            
            if response.result.isFailure
            {
                print(response.result)
                print(response.request)
                print(response.description)
            }
            else
            {
                let objects = response.result.value
                let items = (objects as! NSDictionary).object(forKey: "items")
                print(items)
                if items != nil
                {
                for item in items as! NSArray
                {
                    let tracks = (item as AnyObject).object(forKey: "track")
                    
                    let tempName = (tracks as! NSDictionary).object(forKey: "name")
                    name = tempName as! String
                    
                    let tempExternalURL = (tracks as! NSDictionary).object(forKey: "external_urls")
                    let strippedURL = (tempExternalURL as! NSDictionary).object(forKey: "spotify")
                    externalURL = strippedURL as! String
                    
                    let tempPreviewURL = (tracks as! NSDictionary).object(forKey: "preview_url")
                    if tempPreviewURL as? NSNull == nil
                    {
                    url = tempPreviewURL as! String
                    }
                    
                    fullArray.add([name,url,externalURL])
                }
                }
            }
            completion(fullArray)
            print(fullArray)
        }
        completion(fullArray)
        print(fullArray)
    }
    
    func getSongsForAlbum(_ id : String, completion:(_ songs : NSArray) -> Void)
    {
        let fullArray = NSMutableArray()
        let firstString = id
        let modifiedString = firstString.replace("spotify:album:", withString:"")
        var name = String()
        var url = String()
        var externalURL = String()
        
        Alamofire.request(root + "/v1/albums/" + modifiedString + "/tracks",
                          parameters: ["":""],
                          encoding: URLEncoding.default) .responseJSON { response in
            
            if response.result.isFailure
            {
                print(response.result)
            }
            else
            {
                let objects = response.result.value
                let items = (objects as! NSDictionary).object(forKey: "items")
                for item in items as! NSArray
                {
                    let tempName = (item as! NSDictionary).object(forKey: "name")
                    name = tempName as! String
                    
                    let tempExternalURL = (item as! NSDictionary).object(forKey: "external_urls")
                    let strippedURL = (tempExternalURL as! NSDictionary).object(forKey: "spotify")
                    externalURL = strippedURL as! String

                    let tempPreviewURL = (item as! NSDictionary).object(forKey: "preview_url")
                    url = tempPreviewURL as! String
                    
                    fullArray.add([name,url, externalURL])
                }
            }
        }
        completion(fullArray)
    }
    
    func getAlbums(_ searchString: String, completion:@escaping (_ albums: NSArray) -> Void)
    {
        let fullArray = NSMutableArray()
        var name = String()
        var url = String()
        var id = String()
        var uri = String()
        
        Alamofire.request(root + search + searchString + "&type=album",
                          parameters: ["":""],
                          encoding: URLEncoding.default) .responseJSON { response in
            if response.result.isFailure
            {
                print(response.result)
            }
            else
            {
                let objects = response.result.value
                let albums = (objects as! NSDictionary).object(forKey: "albums")
                let items = (albums as! NSDictionary).object(forKey: "items")
                
                for item in items as! NSArray
                {
                    
                let tempId = (item as! NSDictionary).object(forKey: "id")
                    id = tempId as! String
                    
                let tempImage = (item as! NSDictionary).object(forKey: "images")!
                let firstImage = (tempImage as AnyObject).object(at: 0)
                    
                    let tempURL = (firstImage as! NSDictionary).object(forKey:"url")
                    url = tempURL as! String
                    
                let tempName = (item as! NSDictionary).object(forKey: "name")
                    name = tempName as! String
                    
                let tempURI = (item as! NSDictionary).object(forKey: "uri")
                    uri = tempURI as! String
                    
                    
                    fullArray.add([name,url,id,uri])
                }
            }
            completion(fullArray)
        }
    }
    
    func getPlaylists(_ searchString:String, completion:@escaping (_ playlists:NSArray) -> Void)
    {
        let fullArray = NSMutableArray()
        var name = String() // index 0
        var url = String() // index 1
        var id = String() //index 2
        var playlistID = String()
        
        Alamofire.request(root + search + searchString + "&type=playlist",
                          parameters: ["":""],
                          encoding: URLEncoding.default) .responseJSON { response in
            if response.result.isFailure
            {
                print(response.result)
            }
            else
            {
                if let playlists = response.result.value
                {
                    let playlists = (playlists as AnyObject).object(forKey: "playlists")
                    let items = (playlists! as AnyObject).object(forKey: "items")
                    
                    for item in items as! NSArray
                    
                    {
                        //user id
                        let tempID = (item as AnyObject).object(forKey: "id") as! String
                        id = tempID
                        
                        let tempPlaylistID = (item as AnyObject).object(forKey: "uri") as! String
                        let strippedID = tempPlaylistID.replace("spotify:user:" + tempID + ":playlist:", withString: "")
                        playlistID = strippedID
                        
                        let tempName = (item as AnyObject).object(forKey: "name") as! String
                        name = tempName
                        
                        let images = (item as AnyObject).object(forKey: "images") as! NSArray
                        for image in images
                        {
                            let tempUrl = (image as AnyObject).object(forKey: "url") as! String
                            url = tempUrl
                        }
                        fullArray.add([name,url,id, playlistID])
                    }
                }
                completion(fullArray)
            }
        }
    }
    
    
    func getPhotos(_ completion:@escaping (_ photos: NSArray) -> Void)
    {
        let array = NSMutableArray()
        let params = ["" : ""]
        Alamofire.request(root + "/photos",
                          parameters: params,
                          encoding: URLEncoding.default)
            .responseJSON { response in
                
                if response.result.isFailure
                {
                    print(response.result)
                }
                else
                {
                    if let photos = response.result.value
                    {
                        for photo in photos as! NSArray
                        {
                            let photoURL = (photo as AnyObject).object(forKey: "thumbnailUrl")
                            array.add(photoURL!)
                        }
                    }
                    completion(array)
                }
        }
    }
}









